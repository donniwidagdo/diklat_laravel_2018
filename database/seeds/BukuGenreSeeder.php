<?php

use Illuminate\Database\Seeder;

class BukuGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('buku_genres')->insert([
            [
                'genre_id' => 'satu',
            ],
            [
                'genre_id' =>'dua',
            ],
        ]);
    }
}
