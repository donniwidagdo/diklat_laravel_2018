<?php

use Illuminate\Database\Seeder;

class DeviceElektronikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektroniks')->insert([
            [
                'type_id' => '1',
                'brand'   => 'Sony',
                'series' => 'Vaio',
                'year' => '1998',
            ],
            [
                'type_id' =>'2',
                'brand'   =>'Cisco',
                'series' =>'Catalyst 2960',
                'year' =>'1995',
            ],
        ]);
    }
}
