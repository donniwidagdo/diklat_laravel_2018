<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bukus')->insert([
            [
                'author' => 'How',
                'title'   => 'Morgan Freeman',
                'year' => '1980',
                'genre_id' => '1',
            ],
            [
                'author' =>'What',
                'title'   =>'Donni Saco',
                'year' =>'1998',
                'genre_id' =>'2',
            ],
        ]);
    }
}
