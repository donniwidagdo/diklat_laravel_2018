<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
                 'name' => 'donni',
                 'email' => 'semarangwidagdon@gmail.com',
                 'password' => '$2y$10$y69Dk4opTGov98FjUaYEUO8lrT6dDVjkkG6s0W67vB1Sfc4RthAtK'
            ]);

    }
}
