<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BukuRequest as StoreRequest;
use App\Http\Requests\BukuRequest as UpdateRequest;

/**
 * Class BukuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BukuCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Buku');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/buku');
        $this->crud->setEntityNameStrings('buku', 'bukus');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        
        //Nambah relasi antar tabel
        $this->crud->addField([
        'label' => 'Pilihan Genre',  // <-------  judul inputan
        'type'  => 'select', // <------------- type field nya
        'name'  => 'genre_id', // <------- id form yang tampil
        'entity' => 'Bukugenre', // <------method yang ada dalam method
        'attribute' => 'genre_id', //<---------data dari foregin yang mau ditampilkan
        'model'  => 'App\Models\Bukugenre' //<------model yang dipakai
        ]);

        $this->crud->addField([
            'label' => 'Tanggal Penerbitan',
            'type' =>  'date_picker',
            'name' => 'year',
        ]);
        
        $this->crud->addColumn([
            'name'  => 'row_number',
            'type'  => 'row_number',
            'label' => 'No.',
            'orderable' => false,
        ])->makeFirstColumn();

        $this->crud->addColumn([
            'label' => 'Genre',
            'type'  => 'select',
            'name'  => 'genre_id',
            'entity' => 'Bukugenre',
            'attribute' => 'genre',
            'model' => 'App\Models\Bukugenre'
        ]);

        $this->crud->enableExportButtons();

        // add asterisk for fields that are required in BukuRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
